const fs = require("fs");
const os = require("os");

require('dotenv').config();
const { BD4NRG_TOKEN_ADDRESS } = process.env;

const BD4NRGToken = artifacts.require("BD4NRGToken.sol");
module.exports = function(deployer) {
      deployer.deploy(BD4NRGToken).then((BD4NRGTokenRes) => {
            const address = BD4NRGTokenRes.address;
            setEnvValue("BD4NRG_TOKEN_ADDRESS", address, ".env")
      });
      
}

//const TransactionProcessor = artifacts.require("TransactionProcessor.sol");
//module.exports = function(deployer) {
//      deployer.deploy(TransactionProcessor, BD4NRG_TOKEN_ADDRESS).then((TransactionProcessorRes) => {
//            const address = TransactionProcessorRes.address;
//            setEnvValue("TRANSACTION_PROCESSOR_ADDRESS", address, ".env")
//      });
//}

function setEnvValue(key, value, env_file) {

      // read file from hdd & split if from a linebreak to a array
      const ENV_VARS = fs.readFileSync(env_file, "utf8").split(os.EOL);
  
      // find the env we want based on the key
      const target = ENV_VARS.indexOf(ENV_VARS.find((line) => {
          return line.match(new RegExp(key));
      }));
  
      // replace the key/value with the new value
      ENV_VARS.splice(target, 1, `${key}=${value}`);
  
      // write everything back to the file system
      fs.writeFileSync(env_file, ENV_VARS.join(os.EOL));
  
}
