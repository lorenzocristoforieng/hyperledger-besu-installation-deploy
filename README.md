
# Install Hyperledger Besu Binary
Prerequisites
- [Java JDK 17+](https://www.oracle.com/java/technologies/downloads/)

Download the Besu [packaged binaries](https://github.com/hyperledger/besu/releases).

Unzip the downloaded files and save the besu-```<release>``` directory. \
Add bin/besu to the envirorment variable.

Display Besu command line help to confirm installation:

```bash
bin/besu --help
```

To see different types of installation [click here](https://besu.hyperledger.org/private-networks/get-started/install)

# Create a private network using QBFT

## 1. Create directories

Each node requires a data directory for the blockchain data.

Create directories for your private network, each of the four nodes, and a data directory for each node:

```bash
  QBFT-Network/
├── Node-1
│   ├── data
├── Node-2
│   ├── data
├── Node-3
│   ├── data
└── Node-4
    ├── data
```

## 2. Create the genesis file


```bash
  {
  "genesis": {
    "config": {
      "chainId": 1337,
      "berlinBlock": 0,
      "qbft": {
        "blockperiodseconds": 2,
        "epochlength": 30000,
        "requesttimeoutseconds": 4
      }
    },
    "nonce": "0x0",
    "timestamp": "0x58ee40ba",
    "gasLimit": "0x47b760",
    "difficulty": "0x1",
    "mixHash": "0x63746963616c2062797a616e74696e65206661756c7420746f6c6572616e6365",
    "coinbase": "0x0000000000000000000000000000000000000000",
    "alloc": {
      "fe3b557e8fb62b89f4916b721be55ceb828dbd73": {
        "privateKey": "8f2a55949038a9610f50fb23b5883af3b4ecb3c3bb792cbcefbd1542c692be63",
        "comment": "private key and this comment are ignored.  In a real chain, the private key should NOT be stored",
        "balance": "0xad78ebc5ac6200000"
      },
      "627306090abaB3A6e1400e9345bC60c78a8BEf57": {
        "privateKey": "c87509a1c067bbde78beb793e6fa76530b6382a4c0241e5e4a9ec0a0f44dc0d3",
        "comment": "private key and this comment are ignored.  In a real chain, the private key should NOT be stored",
        "balance": "90000000000000000000000"
      },
      "f17f52151EbEF6C7334FAD080c5704D77216b732": {
        "privateKey": "ae6ae8e5ccbfb04590405997ee2d52d2b330726137b875053c36d94e974d162f",
        "comment": "private key and this comment are ignored.  In a real chain, the private key should NOT be stored",
        "balance": "90000000000000000000000"
      }
    }
  },
  "blockchain": {
    "nodes": {
      "generate": true,
      "count": 4
    }
  }
}
```

## 3. Generate node keys and a genesis file

```bash
  besu operator generate-blockchain-config --config-file=qbftConfigFile.json --to=networkFiles --private-key-file-name=key
```

Besu creates the following in the networkFiles directory:

genesis.json - The genesis file including the extraData property specifying the four nodes are validators.
A directory for each node named using the node address and containing the public and private key for each node.


```bash
networkFiles/
├── genesis.json
└── keys
    ├── 0x438821c42b812fecdcea7fe8235806a412712fc0
    │   ├── key
    │   └── key.pub
    ├── 0xca9c2dfa62f4589827c0dd7dcf48259aa29f22f5
    │   ├── key
    │   └── key.pub
    ├── 0xcd5629bd37155608a0c9b28c4fd19310d53b3184
    │   ├── key
    │   └── key.pub
    └── 0xe96825c5ab8d145b9eeca1aba7ea3695e034911a
        ├── key
        └── key.pub
```
## 4. Copy the genesis file to the QBFT-Network directory and for each node, copy the key files to the data directory for that node.

```bash
  QBFT-Network/
├── genesis.json
├── Node-1
│   ├── data
│   │    ├── key
│   │    ├── key.pub
├── Node-2
│   ├── data
│   │    ├── key
│   │    ├── key.pub
├── Node-3
│   ├── data
│   │    ├── key
│   │    ├── key.pub
├── Node-4
│   ├── data
│   │    ├── key
│   │    ├── key.pub
```

# Run a private network using QBFT
## 5. Start the first node as the bootnode

```bash
  besu --data-path=data --genesis-file=../genesis.json --rpc-http-enabled --rpc-http-api=ETH,NET,QBFT --host-allowlist="*" --rpc-http-cors-origins="all"
```
The command line:

- Specifies the data directory for Node-1 using the --data-path option.
- Enables the JSON-RPC API using the --rpc-http-enabled option.
- Enables the ETH, NET, and QBFT APIs using the --rpc-http-api option.
- Enables all-host access to the HTTP JSON-RPC API using the --host-allowlist option.
- Enables all-domain access to the node through the HTTP JSON-RPC API using the --rpc-http-cors-origins option.
When the node starts, the enode URL displays. Copy the enode URL to specify Node-1 as the bootnode in the following steps.

## 6. Start Node-2

```bash
  besu --data-path=data --genesis-file=../genesis.json --bootnodes=<Node-1 Enode URL> --p2p-port=30304 --rpc-http-enabled --rpc-http-api=ETH,NET,QBFT --host-allowlist="*" --rpc-http-cors-origins="all" --rpc-http-port=8546
```

## 7. Start Node-3

```bash
  besu --data-path=data --genesis-file=../genesis.json --bootnodes=<Node-1 Enode URL> --p2p-port=30305 --rpc-http-enabled --rpc-http-api=ETH,NET,QBFT --host-allowlist="*" --rpc-http-cors-origins="all" --rpc-http-port=8547
```

## 8. Start Node-4

```bash
  besu --data-path=data --genesis-file=../genesis.json --bootnodes=<Node-1 Enode URL> --p2p-port=30306 --rpc-http-enabled --rpc-http-api=ETH,NET,QBFT --host-allowlist="*" --rpc-http-cors-origins="all" --rpc-http-port=8548
```

## 9. Confirm the private network is working
Start another terminal, use curl to call the JSON-RPC API qbft_getvalidatorsbyblocknumber method and confirm the network has four validators:

```bash
  curl -X POST --data '{"jsonrpc":"2.0","method":"qbft_getValidatorsByBlockNumber","params":["latest"], "id":1}' localhost:8545
```
The result displays the four validators:
```bash
  {
  "jsonrpc": "2.0",
  "id": 1,
  "result": [
    "0x73ced0bd3def2e2d9859e3bd0882683a2e6835fb",
    "0x7a175f3542ceb60bf80fb536b3f42e7a30c0a6d7",
    "0x7f6efa6e34f8c9b591a9ad4763e21b3fca31bcd6",
    "0xc64140f1c9d5bb82e54976e568ad39958c3e94be"
  ]
}
```

# Deploy a contract
Go to contracts-deployment folder and install dependencies.

```bash
  npm i
```

In the .env file change the Private and Public key accordly to the Hyperledger Besu installation.

Move the smart contracts you want to deploy inside the contracts folder and modify the 2_deploy_contracts.js file.

deploy the contract in the hyperledger besu network running:
```bash
  truffle migrate --network besuWallet
```
## Deploy the TransactionProcessor and BD4NRGToken contracts

This guide refer to the [TransactionProcessor](https://github.com/engsep/BD4NRG/blob/main/BD4NRG-transaction-processor-master/contracts/TransactionProcessor.sol) contract and the [BD4NRGToken](https://github.com/engsep/BD4NRG/blob/main/BD4NRG-transaction-processor-master/contracts/BD4NRGToken.sol) contract.

Download the two contracts and place them in the contacts-deployment/contracts folder.

### Deploy the BD4NRGToken Contract: 
```bash
  truffle migrate --network besuWallet
```

### Deploy the TransactionProcessor Contract:

In the 2_deploy_contracts.js file comment the BD4NRGToken part and uncomment the transactionProcessor section as follows:

```bash
...

//const BD4NRGToken = artifacts.require("BD4NRGToken.sol");
//module.exports = function(deployer) {
//      deployer.deploy(BD4NRGToken).then((BD4NRGTokenRes) => {
//            const address = BD4NRGTokenRes.address;
//            setEnvValue("BD4NRG_TOKEN_ADDRESS", address, ".env")
//      });
//      
//}

const TransactionProcessor = artifacts.require("TransactionProcessor.sol");
module.exports = function(deployer) {
      deployer.deploy(TransactionProcessor, BD4NRG_TOKEN_ADDRESS).then((TransactionProcessorRes) => {
            const address = TransactionProcessorRes.address;
            setEnvValue("TRANSACTION_PROCESSOR_ADDRESS", address, ".env")
      });
}

...

```

Deploy the contract:
```bash
  truffle migrate --network besuWallet
```

# Tips

## Manually Generate ExtraData to insert in the genesis file

Create a toEncode.json file with the initially validator nodes.

```bash
[
  "0x4592c8e45706cc08b8f44b11e43cba0cfc5892cb",
  "0x06e23768a0f59cf365e18c2e0c89e151bcdedc70",
  "0xc5327f96ee02d7bcbc1bf1236b8c15148971e1de",
  "0xab5e7f4061c605820d3744227eed91ff8e2c8908"
]
```
Generate the ExtraData corrispondig to the toEncode.json file.

```bash
  besu rlp encode --from=toEncode.json --type=QBFT_EXTRA_DATA
```